<?php global $wpdb;
$results = $wpdb->get_col( $wpdb->prepare( "SELECT meta_value as 'values' FROM {$wpdb->prefix}posts JOIN {$wpdb->prefix}postmeta ON {$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.`post_id` WHERE post_type = %s AND meta_key = %s
GROUP BY meta_value",$parameters['post_type'],$parameters['field']), 0 );

return $results;